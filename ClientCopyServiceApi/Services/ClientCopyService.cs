﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ClientCopyServiceApiCommon.Interfaces;
using ClientCopyServiceApiCommon.Models;
using LoggingAsyncCommon.Models;

namespace ClientCopyServiceApi.Services
{
    public class ClientCopyService : IClientCopy
    {
        #region IClientCopy

        /// <summary>
        /// Test method to make sure this is plumbed correctly
        /// </summary>
        /// <returns></returns>
        public async Task<bool> TestMethodAsync(bool x)
        {
            const string methodName = "TestMethodAsync";

            var methodAndArgsMessage =
               $"{methodName}({(x != null ? x.ToString() : "null")})";

            try
            {
                //LogHelper
                //    .LogMessageAsync(
                //        LogType.Trace,
                //        methodName,
                //        $"{methodAndArgsMessage} entered")
                //    .ConfigureAwait(false);


                var response = x;

                //LogHelper
                //    .LogMessageAsync(
                //        LogType.Informational,
                //        methodName,
                //    $"{ methodAndArgsMessage} response: {response}")
                //    .ConfigureAwait(false);

                //LogHelper
                //    .LogMessageAsync(
                //        LogType.Trace,
                //        methodName, $"{methodAndArgsMessage} exited")
                //    .ConfigureAwait(false);

                return response;

            }
            catch (Exception ex)
            {
                //LogHelper.
                //    LogMessageAsync(
                //        LogType.Error,
                //        methodName, $"{methodAndArgsMessage} Exception", ex)
                //    .ConfigureAwait(false);

                throw ex;
            }
        }
        /// <summary>
        /// Function to post one division of a given year from one environment to another.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public async Task<PerformClientCopyResponse> PerformClientCopyAsync(PerformClientCopyRequest x)
        {
            const string methodName = "PerformClientCopyAsync";

            var methodAndArgsMessage =
                $"{methodName}({(x != null ? x.ToString() : "null")})";

            try
            {
                //LogHelper
                //    .LogMessageAsync(
                //        LogType.Trace,
                //        methodName,
                //        $"{methodAndArgsMessage} entered")
                //    .ConfigureAwait(false);

                if (x == null)
                {
                    throw new ArgumentException(
                        nameof(x));
                }

                if (string.IsNullOrWhiteSpace(x.Division))
                {
                    throw new ArgumentException(
                        nameof(x.Division));
                }



                var batchProcessing = new BatchProcessing();

                var batchFileName = batchProcessing.findFile(x.CopyFromTo);

                var tupleSourceDestination = batchProcessing.parseSourceDestination(x.CopyFromTo);
                var response = new PerformClientCopyResponse
                {
                    Division = x.Division,
                    Year = x.Year,
                    CopyFromTo = x.CopyFromTo,
                    CopyWasSuccessful = false,
                    BatchFileInfo = String.Empty,
                    Source = tupleSourceDestination.Item1,
                    Destination = tupleSourceDestination.Item2,
                    DivBlocked = false
                };
                if (!isBlockedDiv(response))
                {
                    if (!batchFileName.Equals(String.Empty))
                    {
                        var batchFilesDirectory = ConfigurationManager.AppSettings["batchFilesNetworkDirectory"];

                        response.BatchFileInfo = batchProcessing.runFile(batchFilesDirectory, x.Division, x.Year, batchFileName);

                        response.CopyWasSuccessful = response.BatchFileInfo == null ? false : true;
                    }
                }                                             
                //LogHelper
                //    .LogMessageAsync(
                //        LogType.Informational,
                //        methodName,
                //    $"{ methodAndArgsMessage} response: {response}")
                //    .ConfigureAwait(false);

                //LogHelper
                //    .LogMessageAsync(
                //        LogType.Trace,
                //        methodName, $"{methodAndArgsMessage} exited")
                //    .ConfigureAwait(false);
                return response;
            }
            catch (Exception ex)
            {
                //LogHelper.
                //    LogMessageAsync(
                //        LogType.Error,
                //        methodName, $"{methodAndArgsMessage} Exception", ex)
                //    .ConfigureAwait(false);

                throw ex;
            }
        }

        public Boolean isBlockedDiv(PerformClientCopyResponse model)
        {
            string div = model.Division;
            if (!div.Equals(String.Empty))
            {
                StreamReader newStream = new StreamReader(Path.Combine(ConfigurationManager.AppSettings["blockListFileDirectory"], ConfigurationManager.AppSettings["fileBlockList"]));
                string singleLine;
                List<string> tempBlockedList = new List<string>();
                while ((singleLine = newStream.ReadLine()) != null)
                {
                    tempBlockedList.Add(singleLine);
                }
                newStream.Close();
                for (int index = 0; index < tempBlockedList.Count; index++)
                {
                    if (div.Substring(0, 2) == tempBlockedList[index] || div == tempBlockedList[index])
                    {
                        model.DivBlocked = true;
                        model.CopyWasSuccessful = false;
                        return true;
                    }
                }
            }            
            return false;
        }
        #endregion
    }

    public class BatchProcessing
    {
        public string runFile(string directory, string division, int year, string batchfileName)
        {
            var proc = new Process();
            proc.StartInfo.WorkingDirectory = directory;
            proc.StartInfo.FileName = batchfileName;  //  fileName passed in from the Copy Click Event
            proc.StartInfo.Arguments = $"{division} {year}"; // @"" + div + " " + year;  //  div & year come from user input
            proc.StartInfo.CreateNoWindow = false;
            proc.Start();            
            proc.WaitForExit();

            return Path.Combine(new string[] { directory, batchfileName });
        }

        public string findFile(CopyFromTo copyFromTo)
        {
            var response = String.Empty;

            switch (copyFromTo)
            {
                case CopyFromTo.ProductionToTest:
                    response = ConfigurationManager.AppSettings["ProductionToTest"];
                    break;
                case CopyFromTo.ProductionToQA:
                    response = ConfigurationManager.AppSettings["ProductionToQA"];
                    break;
                case CopyFromTo.ProductionToXPay:
                    response = ConfigurationManager.AppSettings["ProductionToXPay"];
                    break;
                case CopyFromTo.TestToQA:
                    response = ConfigurationManager.AppSettings["TestToQA"];
                    break;
                case CopyFromTo.TestToXPay:
                    response = ConfigurationManager.AppSettings["TestToXPay"];
                    break;
                default:
                    response = String.Empty;
                    break;
            }

            return response;

        }

        public Tuple<string, string> parseSourceDestination(CopyFromTo copyFromTo)
        {
            var response = new Tuple<string, string>(string.Empty, string.Empty);

            switch (copyFromTo)
            {
                case CopyFromTo.ProductionToTest:
                    response = new Tuple<string, string>("Production", "Test");
                    break;
                case CopyFromTo.ProductionToQA:
                    response = new Tuple<string, string>("Production", "QA");
                    break;
                case CopyFromTo.ProductionToXPay:
                    response = new Tuple<string, string>("Production", "XPay");
                    break;
                case CopyFromTo.TestToQA:
                    response = new Tuple<string, string>("Test", "QA");
                    break;
                case CopyFromTo.TestToXPay:
                    response = new Tuple<string, string>("Test", "XPay");
                    break;
                default:
                    response = new Tuple<string, string>(string.Empty, string.Empty);
                    break;
            }

            return response;
        }
    }
}
