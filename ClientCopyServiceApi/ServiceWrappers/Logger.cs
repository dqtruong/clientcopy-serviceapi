﻿using LoggingAsyncCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ClientCopyApi.ServiceWrappers
{
    public static class Logger
    {
        
        private static int AppId = 73; // service Logging Name "ClientCopyService"
        private static readonly IOILoggingClientAsync.Logging _logger;

        static Logger()
        {
            _logger = new IOILoggingClientAsync.Logging("NetTcpBinding_ILoggingAsync");
        }

        public static void LogInfo(string comment, string procedureName, string moduleName, string tag = null )
        {
            var message = new ErrorMsg
            {
                Comment = comment,
                ModuleName = moduleName,
                ProcedureName = procedureName,
                Tag = tag,
                MsgDateTime = DateTime.Now,
                LogType = LogType.Informational,
                AppID = AppId
            };

            WriteErrorAsync(message).ConfigureAwait(false);
        }

        public static void LogTrace(string comment, string procedureName, string moduleName, string tag = null)
        {
            var message = new ErrorMsg
            {
                Comment = comment,
                ModuleName = moduleName,
                ProcedureName = procedureName,
                Tag = tag,
                MsgDateTime = DateTime.Now,
                LogType = LogType.Trace,
                AppID = AppId
            };

            WriteErrorAsync(message).ConfigureAwait(false);
        }

        public static void LogError(string exception, string stackTrace, string procedureName, string moduleName, string comment = null, string tag = null)
        {
            var message = new ErrorMsg
            {
                Exception = exception,
                StackTrace = stackTrace,
                ModuleName = moduleName,
                ProcedureName = procedureName,
                Tag = tag,
                MsgDateTime = DateTime.Now,
                LogType = LogType.Error,
                AppID = AppId
            };
            WriteErrorAsync(message).ConfigureAwait(false);
        }

        public static void LogAppTrace(string method, string moduleName, string tag, DateTime msgDateTime, double requestTime, HttpRequestMessage request)
        {
            var requestUrl = ((System.Web.HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.Url;
            if (requestUrl == null) return;
            var requestUserHostAddress = ((System.Web.HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            if (requestUserHostAddress == null) return;

            var message = new AppTraceMsg
            {
                Method = method,
                Tag = tag,
                ModuleName = moduleName,
                MsgDateTime = msgDateTime,
                RequestTime = requestTime,
                LogType = LogType.AppTrace,
                AppID = AppId,
                URL = requestUrl.ToString(),
                Scope = "APIServer",
                IPAddress = requestUserHostAddress.ToString(),
            };

            WriteAppTraceAsync(message).ConfigureAwait(false);
        }

        public static void LogExternalMessage(ErrorMsg msg)
        {
            msg.AppID = 74; // UI Loging Name "ClientCopyUI"
            WriteErrorAsync(msg).ConfigureAwait(false);
        }

        public static void LogExternalTrace(AppTraceMsg msg)
        {
            //UI ID
            msg.AppID = 74;  // UI Loging Name "ClientCopyUI"
            WriteAppTraceAsync(msg).ConfigureAwait(false);
        }

        private static async Task WriteErrorAsync(ErrorMsg msg)
        {
            await _logger.WriteErrorAsync(msg).ConfigureAwait(false);
        }

        private static async Task WriteAppTraceAsync(AppTraceMsg msg)
        {
            await _logger.WriteAppTraceAsync(msg).ConfigureAwait(false);
        }
    }
}