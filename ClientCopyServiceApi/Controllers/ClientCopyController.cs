﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using ClientCopyApi.Controllers;
using ClientCopyServiceApiCommon.Models;
using Newtonsoft.Json;

namespace ClientCopyServiceApi.Controllers
{
    public class ClientCopyController : BaseController
    {
        ClientCopyServiceApi.Services.ClientCopyService _srv = new Services.ClientCopyService();

        [HttpPost]
        [Route("TestMethodAsync")]
        public async Task<Boolean> TestMethodAsync(bool input)
        {
            try
            {
                var rslt = await _srv.TestMethodAsync(input).ConfigureAwait(false);
                return rslt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("PerformClientCopyAsync")]
        public async Task<JsonResult<PerformClientCopyResponse>> PerformClientCopyAsync(PerformClientCopyRequest input)
        {
            try
            {
                PerformClientCopyResponse rslt = await _srv.PerformClientCopyAsync(input).ConfigureAwait(false);
                return Json(rslt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public async Task<HttpResponseMessage> PerformClientCopyAsync(PerformClientCopyRequest input)
        //{
        //    try
        //    {
        //        var rslt = await _srv.PerformClientCopyAsync(input).ConfigureAwait(false);
        //        return Request.CreateResponse(rslt);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
        //    }
        //}
    }
}
