﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ClientCopyApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BaseController : ApiController
    {
        public DateTime startTime = DateTime.Now;

        internal string GetRequesterIp(HttpRequestMessage message)
        {
            return ((HttpContextWrapper)message.Properties["MS_HttpContext"]).Request.UserHostAddress;
        }
    }
}
