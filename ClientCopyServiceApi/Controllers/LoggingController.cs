﻿using ClientCopyApi.ServiceWrappers;
using LoggingAsyncCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ClientCopyApi.Controllers
{
    [RoutePrefix("logging")]
    public class LoggingController : BaseController
    {

        [HttpPost]
        [Route("message")]
        public HttpResponseMessage message([FromBody] ErrorMsg msg)
        {

            try
            {
                Logger.LogExternalMessage(msg);
                return Request.CreateResponse(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }

        [HttpPost]
        [Route("trace")]
        public HttpResponseMessage trace([FromBody] AppTraceMsg msg)
        {

            try
            {
                Logger.LogExternalTrace(msg);
                return Request.CreateResponse(HttpStatusCode.OK, "Success");
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }
    }
}
