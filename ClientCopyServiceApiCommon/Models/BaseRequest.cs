﻿using System.ComponentModel.DataAnnotations;

namespace ClientCopyServiceApiCommon.Models
{
    public enum CopyFromTo
    {
        ProductionToTest = 0,

        ProductionToXPay = 1,

        ProductionToQA = 2,

        TestToQA = 3,

        TestToXPay = 4
    }

    public class BaseBatchRequest
    {
        [Required]
        [StringLength(5, MinimumLength = 5)]
        [RegularExpression("[A-Z][A-Z0-9]{4}")]     
        [DataType((DataType.Text))]
        public string Division { get; set; }

        [Required]
        [Range(2010, 2100)]
        public int Year { get; set; }

        [Required]
        public CopyFromTo CopyFromTo { get; set; }

        public override string ToString()
        {
            return $"Division={Division},Year={Year},CopyFromTo={CopyFromTo}";
        }
    }

    public class PerformClientCopyRequest : BaseBatchRequest
    {

    }

}
