﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClientCopyServiceApiCommon.Models
{
    public class PerformClientCopyResponse : BaseBatchRequest
    {
        [Required]
        public bool CopyWasSuccessful { get; set; }

        public string BatchFileInfo { get; set; }

        public bool DivBlocked { get; set; }

        [Required]
        public string Source { get; set; }

        [Required]
        public string Destination { get; set; }

        public override string ToString()
        {
            return $"CopyWasSuccessful={CopyWasSuccessful},BatchFileInfo={BatchFileInfo},Division={Division},Year={Year},CopyFromTo={CopyFromTo},Source={Source},Destination={Destination}";
        }
    }
}
