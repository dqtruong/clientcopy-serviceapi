﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ClientCopyServiceApiCommon.Models;

namespace ClientCopyServiceApiCommon.Interfaces
{
   public interface IClientCopy
   {
        /// <summary>
        /// Test method to make sure this is plumbed correctly
        /// </summary>
        /// <returns></returns>            
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "testMethodAsync")]
        Task<bool> TestMethodAsync(bool x);

        /// <summary>
        /// Function to post one division of a given year from one environment to another. 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>           
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "performClientCopy")]            
        Task<PerformClientCopyResponse> PerformClientCopyAsync(PerformClientCopyRequest x);

    }
}

